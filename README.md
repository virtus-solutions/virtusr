
# virtusr

<!-- badges: start -->
[![Lifecycle: experimental](https://img.shields.io/badge/lifecycle-experimental-orange.svg)](https://lifecycle.r-lib.org/articles/stages.html#experimental)
<!-- badges: end -->

`virtusr`, is an R package tailored to facilitate working with New Zealand data. 
Born out of our own experiences and needs, virtusr provides a collection of practical tools designed to simplify your data analysis tasks when working with data in New Zealand. 
Whether you're mapping data using dasymetric techniques, organising New Zealand regions from north to south, or classifying data into North Island and South Island categories, virtusr offers a helping hand. 

## Installation

You can install the development version of virtusr like so:

``` r
devtools::install_git("https://gitlab.com/virtus-solutions/virtusr.git")
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(virtusr)
## basic example code
```

